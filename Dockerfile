FROM node:6

RUN mkdir /code
WORKDIR /code

# Install Node deps
RUN npm install -g webpack webpack-dev-server typings karma-cli --loglevel warn
COPY package.json /code/package.json
RUN npm install --loglevel warn

VOLUME /code/node_modules
