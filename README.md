Javascript port of simple-asymmetric-python

The goal is to create a "developer usable" crypto library as a wrapper around various more trusted libraries in various languages and provide a consistent and fully interoperable api.

# Status

Feature complete - but very new. Use at your own risk. I strongly recommend having the code audited before using in a production environment. Want to split the cost of a security audit? Open an issue and let's talk.

# Developing

We use webpack, docker, typescript, and jasmine. Jasmine must run in a real browser as it requires web crpyto.

1. Install Docker and Docker Compose
2. `docker-compose up`
3. Go to http://localhost:8080/ to view passing tests

## CI

Saucelabs is used for CI - since we can't run subtle crypto in phantomjs or node. See karma.conf.js and .gitlab-ci.yml

Everything runs from Docker - so you should be able to run something like 

1. `docker-compose run --rm app bash`
2. Set saucelab env vars (username and access key)
3. `karma start`

## Releasing to NPM

Compile typescript and npm publish.

## Note on style

snake_case is used often. This is because the library was developed in Python first and it provides some consistency. Use snake_case when matching python names. Otherwise use best practices JavaScript which is typically camelCase.

simple_asym/crypto.ts should contain only wrappers around existing crypto libraries
simple_asym/asymmetric_encryption.ts should contain only application logic. It should never contain any crypto. Never use window.crypto or related functions here.

# How

[Subtle Crypto](https://developer.mozilla.org/en-US/docs/Web/API/SubtleCrypto) powers all the real cryptography however we use a few libraries in addition.

- Node Forge - To convert web crypto generated Private jwk to an encrypted PEM
- pem-jwk - To convert web crypto generated Public jwk to PEM

Basically Subtle Crypto can't deal with PEM, but PEM is a popular storage for keys. Implimenting our own logic to convert would be more error prone than using existing libraries. That said Forge is a pretty giant dependency for doing so little. Merge requests welcome.

# Supported platforms

Tested against latest Chrome and Firefox. Does not run in Node due to dependency on web crypto. Should run in Electron (TODO verify).

# Contributing

Some ideas for improving

- Remove node forge dependency (so build a lighter weight import and export encrypted PEM)
- Make it somehow work in Node.
- How to use this in React Native?
- Document simply-asymmetic-* specification
- Security auditing

Merge requests require tests.

No core features will be accepted without discussion and inclusion on simple-asymmetic-python. For example adding an alternative encryption algorithm would be rejected without much discussion and a python port. Something like node support would be gladly accepted.
