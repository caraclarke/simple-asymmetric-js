module.exports = function(config) {
  var customLaunchers = {
    sl_chrome: {
      base: 'SauceLabs',
      browserName: 'chrome',
      platform: "Linux",
      version: '48'
    }
  }

  config.set({
    sauceLabs: {
        testName: 'Web App Unit Tests'
    },
    customLaunchers: customLaunchers,
    browsers: Object.keys(customLaunchers),
    reporters: ['dots', 'saucelabs'],
    files: [
      { pattern: 'index.js', watched: false }
    ],
    frameworks: ['jasmine'],
    preprocessors: {
      'index.js': ['webpack'],
    },
    singleRun: true,
    webpack: {
      module: {
        loaders: [
          { test: /\.tsx?$/, loader: 'ts-loader' }
        ],
      },
      watch: true,
      resolve: {
        extensions: ['', '.webpack.js', '.web.js', '.ts', '.tsx', '.js']
      },
    },
    webpackServer: {
      noInfo: true,
    },
    captureTimeout: 120000,
    browserNoActivityTimeout: 60000,
    colors: true,
    logLevel: config.LOG_INFO
  });
};
