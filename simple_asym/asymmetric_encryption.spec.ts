import Asym from "./asymmetric_encryption.ts"
import { generateKeys } from "./crypto.ts"
import {
    UNENCRYPTED_PRIVATE_KEY,
    PUBLIC_KEY,
    PY_ENCRYPTED_PRIVATE_KEY,
    PY_PASSPHRASE,
    AES_KEY,
    PYTHON_DATA
} from "./test_data.ts" 


var aesKey = 'uaBbv71UYwAndWfYRGO6lqgkJTylUdqLzCGJ7xLyvq4='



describe('Asym class can', () => {
    var asym;

    beforeEach(function() {
        asym = new Asym()
    });

    it('make RSA key', (done) => {
        asym.make_rsa_keys().then(keys => {
            expect(keys[0]).toContain('-----BEGIN RSA PRIVATE KEY-----')
            expect(keys[1]).toContain('-----BEGIN RSA PUBLIC KEY-----')
            done();
        })
    })

    it('make RSA key with passphrase', done => {
        asym.make_rsa_keys('123456').then(keys => {
            expect(keys[0]).toContain('-----BEGIN ENCRYPTED PRIVATE KEY-----')
            done()
        })
    })

    it('make RSA key with generated passphrase', done => {
        asym.make_rsa_keys_with_passphrase().then(keys => {
            expect(keys[0]).toContain('-----BEGIN ENCRYPTED PRIVATE KEY-----')
            done()
        })
    })

    it('set a web crypto key object', done => {
        generateKeys().then(keys => {
            asym.set_public_key(keys.publicKey).then( key => {
                expect(key).toBe(keys.publicKey)
                done()
            })
        })
    })

    it('set a PEM public key', done => {
        let asymKey = asym.set_public_key(PUBLIC_KEY).then( key => {
            expect(key.type).toBe("public")
            done()
        })
    })

    it('set a PEM private key', done => {
        asym.set_private_key(UNENCRYPTED_PRIVATE_KEY).then( key => {
            expect(key.type).toBe("private")
            done()
        })
    })

    it('set a encrypted PEM private key', done => {
        asym.set_private_key(PY_ENCRYPTED_PRIVATE_KEY, PY_PASSPHRASE).then( key => {
            expect(key.type).toBe("private")
            done()
        })
    })

    it('encrypt and decrypt using RSA public key', done => {
        let plaintext = 'hello'
        asym.set_private_key(UNENCRYPTED_PRIVATE_KEY).then( key => {
            asym.set_public_key(PUBLIC_KEY).then( key => {
                asym.rsa_encrypt(plaintext, true).then( ciphertext => {
                    expect(ciphertext).not.toBe(plaintext)
                    asym.rsa_decrypt(ciphertext, true).then( decryptedPlaintext => {
                        expect(decryptedPlaintext).toBe(plaintext)
                        done()
                    })
                })
            })
        })
    })

    it('generate AES key', () => {
        let key = asym.make_aes_key()
        expect(asym.aes_key).toBeDefined()
        expect(typeof(key)).toBe('string')
    })

    it('set AES key', () => {
        asym.set_aes_key(AES_KEY)
        expect(asym.aes_key).toBeDefined()
    })

    // it('AES key should be undefined if no string passed in', function() {
    //     expect(asym.set_aes_key()).toBeUndefined();
    // })

    it('throws undefined error if AES key not set', () => {
      var test_key = () => {
        expect(asym.set_aes_key()).toBeUndefined()
      }
      expect(test_key).toThrow(new Error("no property passed to asym.set_aes_key() - AES Key undefined"))
    })

    it('get and set an encrypted shared AES key', done => {
        asym.set_aes_key(AES_KEY)
        asym.get_encrypted_aes_key(PUBLIC_KEY, true).then(encryptedKey => {
            expect(typeof(encryptedKey)).toBe('string')
            asym = new Asym()
            asym.set_private_key(UNENCRYPTED_PRIVATE_KEY).then(() => {
                asym.set_aes_key_from_encrypted(encryptedKey, true).then( () => {
                    expect(asym.aes_key).toBe(AES_KEY)
                    done()
                })
            })
        })
    })

    it('encrypt and decrypt using AES key', () => {
        let message = 'hello'
        asym.set_aes_key(AES_KEY)
        let ciphertext = asym.encrypt(message)
        let decrypted = asym.decrypt(ciphertext)
        expect(decrypted).toBe(message)
    })

    it('bob and alice integration test', (done) => {
        let bob = new Asym()
        let alice = new Asym()
        let msg = "hello"
        bob.make_rsa_keys()
        alice.make_rsa_keys().then(() => {
            bob.make_aes_key()
            bob.get_encrypted_aes_key(alice.public_key).then((shared_encrypted_aes) => {
                alice.set_aes_key_from_encrypted(shared_encrypted_aes).then(() => {
                    let msg_ciphertext = bob.encrypt(msg)
                    expect(msg_ciphertext).not.toBe(msg)
                    let decrypted_msg = alice.decrypt(msg_ciphertext)
                    expect(decrypted_msg).toBe(msg)
                    done()
                })
            })
        })
    })

    it('bob and alice integration test with base64', (done) => {
        let bob = new Asym()
        let alice = new Asym()
        let msg = "hello"
        bob.make_rsa_keys()
        alice.make_rsa_keys().then(() => {
            bob.make_aes_key()
            bob.get_encrypted_aes_key(alice.public_key, true).then((shared_encrypted_aes) => {
                alice.set_aes_key_from_encrypted(shared_encrypted_aes, true).then(() => {
                    let msg_ciphertext = bob.encrypt(msg)
                    expect(msg_ciphertext).not.toBe(msg)
                    let decrypted_msg = alice.decrypt(msg_ciphertext)
                    expect(decrypted_msg).toBe(msg)
                    done()
                })
            })
        })
    })

    it('bob and alice integration test with python generated data', (done) => {
        let bob = new Asym()
        let alice = new Asym()
        let msg = "hello"

        let asymKey = bob.set_private_key(PYTHON_DATA.bob_private).then(() => {
        })

        bob.make_rsa_keys()
        alice.make_rsa_keys().then(() => {
            bob.make_aes_key()
            bob.get_encrypted_aes_key(alice.public_key).then((shared_encrypted_aes) => {
                alice.set_aes_key_from_encrypted(shared_encrypted_aes).then(() => {
                    let msg_ciphertext = bob.encrypt(msg)
                    expect(msg_ciphertext).not.toBe(msg)
                    let decrypted_msg = alice.decrypt(msg_ciphertext)
                    expect(decrypted_msg).toBe(msg)
                    done()
                })
            })
        })
    })
})
