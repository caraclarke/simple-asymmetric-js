import {
    MissingAESException,
    MissingRSAPublicException,
    MissingRSAPrivateException
} from './exceptions.ts'
import {
    generateKeys,
    exportPrivateKey,
    exportPublicKey,
    importPublicKey,
    importPrivateKey,
    rsaEncrypt,
    rsaDecrypt,
    getRandomValues,
    generateFernetKey,
    generateFernetToken,
    aesEncrypt,
    aesDecrypt
} from './crypto.ts'
import { ab2str } from './utils.ts'

const DEFAULT_MODULUS = 4096;


export default class Asym {
    public public_key:CryptoKey;
    private _private_key:CryptoKey;
    private aes_key:string;

    public make_rsa_keys(
        password?:string,
        bits=DEFAULT_MODULUS
    ): Promise<[string, string]> {
        let self = this;
        return new Promise((resolve, reject) => {
            generateKeys().then(keys => {
                self.public_key = keys.publicKey
                self._private_key = keys.privateKey

                // Now export as PEM
                exportPrivateKey(keys.privateKey, password).then(privateKeyPem => {
                    exportPublicKey(keys.publicKey).then(publicKeyPem => {
                        resolve([privateKeyPem, publicKeyPem]);
                    });
                });
            })
            .catch( (err) => {
                reject(err)
            })
        })
    }

    public make_rsa_keys_with_passphrase(
        bits=DEFAULT_MODULUS
    ): Promise<[CryptoKey, CryptoKey, string]> {
        return new Promise((resolve, reject) => {
            let passphrase = this.generate_passphrase()
            this.make_rsa_keys(passphrase, bits).then(keys => {
                keys.push(passphrase)
                resolve(keys);
            })
        })
    }

    public set_public_key(public_key: string | CryptoKey): Promise<CryptoKey> {
        let self = this
        return new Promise((resolve, reject) => {
            if (typeof public_key === "string") {
                importPublicKey(public_key).then(publicKey => {
                    self.public_key = publicKey
                    resolve(self.public_key)
                })
            }
            if (public_key instanceof CryptoKey) {
                self.public_key = public_key
                resolve(self.public_key)
            }
        })
    }

    public set_private_key(private_key: string | CryptoKey, passphrase?: string): Promise<CryptoKey> {
        let self = this
        return new Promise((resolve, reject) => {
            if (typeof private_key === "string") {
                importPrivateKey(private_key, passphrase).then(privateKey => {
                    self._private_key = privateKey
                    resolve(self._private_key)
                })
            }
            if (private_key instanceof CryptoKey) {
                self._private_key = private_key
                resolve(self._private_key)
            }

        })
    }

    public rsa_encrypt(plaintext: string, use_base64 = false): Promise<string> {
        /* Encrypt with RSA public key. Returns ciphertext. */
        let self = this
        return new Promise((resolve, reject) => {
            rsaEncrypt(self.public_key, plaintext, use_base64).then(ciphertext => {
                resolve(ciphertext)
            })
        })
    }

    public rsa_decrypt(ciphertext: string, use_base64 = false): Promise<string> {
        let self = this
        return new Promise((resolve, reject) => {
            rsaDecrypt(self._private_key, ciphertext, use_base64).then(plaintext => {
                resolve(plaintext)
            })
        })
    }

    public make_aes_key(): string {
        /* Generate a new AES key and set it to be used as the fernet token
         * Returns the key */
        let key = generateFernetKey()
        this.set_aes_key(key)
        return key
    }

    public set_aes_key(aes_key: string) {
        this.aes_key = aes_key

        if (!this.aes_key) {
          throw new Error("no property passed to asym.set_aes_key() - AES Key undefined")
        }
    }

    public get_encrypted_aes_key(public_key: string | CryptoKey , use_base64 = false): Promise<string> {
        let self = this
        return new Promise((resolve, reject) => {
            let asym = new Asym()
            asym.set_public_key(public_key).then(() => {
                asym.rsa_encrypt(self.aes_key, use_base64).then(encryptedKey => {
                    resolve(encryptedKey)
                })
            })
        })
    }

    public set_aes_key_from_encrypted(ciphertext: string, use_base64 = false): Promise<any> {
        /* Set aes_key from an encrypted key
        A shortcut method for receiving a AES key that was encrypted for our
        RSA public key

        @param ciphertext: Encrypted version of the key (bytes or base64 string)
        @param use_base64: If true, decode the base64 string
        */
        let self = this
        return new Promise((resolve, reject) => {
            self.rsa_decrypt(ciphertext, use_base64).then(aesKey => {
                self.set_aes_key(aesKey)
                resolve()
            })
        })
    }

    public encrypt(plaintext: string): any {
        return aesEncrypt(this.aes_key, plaintext)
    }

    public decrypt(ciphertext: string): string {
        return aesDecrypt(this.aes_key, ciphertext)
    }

    private generate_passphrase(): string {
        let random = getRandomValues(32)
        return String.fromCharCode.apply(null, random)
    }
}
