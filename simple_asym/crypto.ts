// Workaround for https://github.com/digitalbazaar/forge/issues/198
// Used only for importing and exportings RSA keys
var forge = require('../libs/forge.bundle.js')

// Not sure why this doesn't work in typescript
//import { jwk2pem, pem2jwk } from 'pem-jwk'
var { jwk2pem, pem2jwk } = require('pem-jwk')
import { str2ab, ab2str } from './utils.ts'

// For base64 conversion (more reliable than window.atob)
declare var Unibabel:any
require('unibabel')

// Symmetrical encrpytion
var fernet = require('fernet')

// This can be removed in typescript 2.0
declare var crypto

const DEFAULT_MODULUS = 2048


export const generateKeys = (
    modulus = DEFAULT_MODULUS
): Promise<{'privateKey': CryptoKey, 'publicKey': CryptoKey}> => {
    return new Promise(
        (resolve, reject) => {
            crypto.subtle.generateKey(
                {
                    name: "RSA-OAEP",
                    modulusLength: DEFAULT_MODULUS, //2048 or 4096
                    publicExponent: new Uint8Array([0x01, 0x00, 0x01]),
                    hash: {name: "SHA-256"}, //"SHA-1", "SHA-256", "SHA-384", or "SHA-512"
                },
                true, //whether the key is extractable
                ["encrypt", "decrypt"]
            )
            .then( (key) => {
                resolve(key)
            })
            .catch( (err) => {
                reject(err)
            });
        }
    );
}


export const exportPrivateKey = (key, passphrase?: string): Promise<string> => {
    /* Export web crypto key as PEM with optional passphrase */
    return new Promise((resolve, reject) => {
        crypto.subtle.exportKey("pkcs8", key).then(exportedKey => {
            // Use forge to convert to PEM
            let buffer = new forge.util.ByteBuffer(exportedKey);
            let asn1 = forge.asn1.fromDer(buffer);
            let privateKey = forge.pki.privateKeyFromAsn1(asn1);
            if (passphrase) {
                var pem = forge.pki.encryptRsaPrivateKey(privateKey, passphrase);
            } else {
                var pem = forge.pki.privateKeyToPem(privateKey);
            }
            resolve(pem);
        }, (e) => {
            reject(e);
        });
    });
};


export const exportPublicKey = (key): Promise<string> => {
    /* Export web crpyto key as PEM */
    return new Promise((resolve, reject) => {
        crypto.subtle.exportKey("jwk", key).then(exportedKey => {
            let pem = jwk2pem(exportedKey);
            resolve(pem);
        });
    });
};

export const importPublicKey = (key: string): Promise<CryptoKey> => {
    /* Import PEM as web crypto object */
    return new Promise((resolve, reject) => {
        crypto.subtle.importKey(
            "jwk",
            pem2jwk(key),
            {
                name: "RSA-OAEP",
                hash: {name: "SHA-256"}
            },
            false,
            ["encrypt"]
        ).then(publicKey => {
            resolve(publicKey)
        })
    })
}

export const importPrivateKey = (key: string, passphrase?: string): Promise<CryptoKey> => {
    /* Import PEM as web crypto object with optional passphrase */
    return new Promise((resolve, reject) => {
        if (typeof passphrase != 'undefined') {
            // Decrypt the private key using forge
            let forgePrivateKey = forge.pki.decryptRsaPrivateKey(key, passphrase)
            key = forge.pki.privateKeyToPem(forgePrivateKey)
        }
        crypto.subtle.importKey(
            "jwk",
            pem2jwk(key),
            {
                name: "RSA-OAEP",
                hash: {name: "SHA-256"}
            },
            false,
            ["decrypt"]
        ).then(privateKey => {
            resolve(privateKey)
        })
    })
}


export const rsaEncrypt = (publicKey: CryptoKey, plaintext: string, base64 = false): Promise<string> => {
    return new Promise((resolve, reject) => {
        let buffer = str2ab(plaintext)
        crypto.subtle.encrypt(
            {
                name: "RSA-OAEP",
                iv: crypto.getRandomValues(new Uint8Array(16)),
            },
            publicKey,
            buffer
        )
        .then(ciphertext => {
            if (base64 === true) {
                resolve(Unibabel.bufferToBase64(new Uint8Array(ciphertext)))
            } else {
                resolve(ab2str(ciphertext))
            }
        })
    })
}


export const rsaDecrypt = (privateKey: CryptoKey, ciphertext: string, base64 = false): Promise<string> => {
    /* Decrypt using private key. Returns plaintext. */
    return new Promise((resolve, reject) => {
        let buffer
        if (base64 === true) {
            buffer = Unibabel.base64ToBuffer(ciphertext)
        } else {
            buffer = str2ab(ciphertext)
        }
        crypto.subtle.decrypt(
            {
                name: "RSA-OAEP"
            },
            privateKey,
            buffer
        )
        .then(plaintext => {
            resolve(ab2str(plaintext))
        })
    })
}


export const getRandomValues = (bytes = 32) => {
    return crypto.getRandomValues(new Uint8Array(bytes))
}


export const generateFernetKey = () => {
    return Unibabel.arrToBase64(getRandomValues())
}


export const generateFernetToken = (key?: string) => {
    if (typeof key == 'undefined') {
        key = generateFernetKey()
    }
    let secret = new fernet.Secret(key)
    return new fernet.Token({
        secret: secret,
        time: new Date(),
        iv: getRandomValues(16)
    })
}


export const aesEncrypt = (key: string, plaintext: string): any => {
    let token = generateFernetToken(key)
    let ciphertext = token.encode(plaintext)
    return ciphertext
}

export const aesDecrypt = (key: string, ciphertext: string): string => {
    let secret = new fernet.Secret(key)
    let token = new fernet.Token({
        secret: secret,
        token: ciphertext,
        ttl: 0
    })
    return token.decode()
}
