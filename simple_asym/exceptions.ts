/**
 * Errors
 */

/**
 *  * Error for missing aes key
 *   * @type {Error}
 *    */
export var MissingAESException:Error = new Error('Missing AES key. Set or generate one');

/**
 *  * Error for missing public key
 *   * @type {Error}
 *    */
export var MissingRSAPublicException:Error = new Error('Missing public RSA key. Set or generate one to use RSA encryption');

/**
 *  * Error for missing private key
 *   * @type {Error}
 *    */
export var MissingRSAPrivateException:Error = new Error('Missing private RSA key. Set or generate one to use RSA decrypt');
