declare var Unibabel:any
require('unibabel')


export const str2ab = (str:string): ArrayBuffer => {
    let buf = new ArrayBuffer(str.length*2)
    let bufView = new Uint16Array(buf)
    for (let i=0, strLen=str.length; i<strLen; i++) {
        bufView[i] = str.charCodeAt(i)
    }
    return buf
}


export const ab2str = (buffer: ArrayBuffer): string => {
    return String.fromCharCode.apply(null, new Uint16Array(buffer));
}
